package co.dwent.BlockRespawn;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class BlockRespawn extends JavaPlugin implements Listener
{
	public static int respawnTimer;
	public static int longHour;
	public static int longMin;
	public static int longSec;
	public Logger pluginLog = Logger.getLogger("Minecraft");
	public Map<Player, Boolean> blockClickEnabled = new HashMap<Player,Boolean>();

	public void onEnable() {
		this.pluginLog.info("[BlockRespawn v" + getDescription().getVersion() + "] has been enabled!");
		getServer().getPluginManager().registerEvents(this, this);
		getConfig().options().copyDefaults(true);
		startRespawnTimer();
		saveConfig();
	}

	public void onDisable() {
		this.pluginLog.info("[BlockRespawn] has been disabled!");
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		
		if (!this.blockClickEnabled.containsKey(player)) {
			this.blockClickEnabled.put(player, Boolean.valueOf(false));
		}

		if ((event.getAction() == Action.RIGHT_CLICK_BLOCK) && (((Boolean)this.blockClickEnabled.get(player)).booleanValue())) {
			player.sendMessage(ChatColor.DARK_RED + "You have set the new respawn!");

			getConfig().set("BlockInfo.BlockID", Integer.valueOf(event.getClickedBlock().getTypeId()));
			getConfig().set("BlockInfo.Type", Byte.valueOf(event.getClickedBlock().getData()));
			getConfig().set("BlockInfo.X", Integer.valueOf(event.getClickedBlock().getX()));
			getConfig().set("BlockInfo.Y", Integer.valueOf(event.getClickedBlock().getY()));
			getConfig().set("BlockInfo.Z", Integer.valueOf(event.getClickedBlock().getZ()));
			this.blockClickEnabled.put(player, Boolean.valueOf(false));
			saveConfig();
			reloadConfig();

			if (getServer().getScheduler().isCurrentlyRunning(respawnTimer)) {
				getServer().getScheduler().cancelTask(respawnTimer);
			}
		}
	}

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		int blockID = getConfig().getInt("BlockInfo.BlockID");
		int blockX = getConfig().getInt("BlockInfo.X");
		int blockY = getConfig().getInt("BlockInfo.Y");
		int blockZ = getConfig().getInt("BlockInfo.Z");

		Block block = event.getBlock();
		
		if ((block.getX() == blockX) && (block.getY() == blockY) && (block.getZ() == blockZ) && (block.getTypeId() == blockID)) {
			startRespawnTimer();
		}
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (cmd.getName().equalsIgnoreCase("blockRespawn")) {
			
			Player player = (Player)sender;
			
			if ((!player.isOp()) && ((player instanceof Player))) {
				player.sendMessage(ChatColor.RED + "You do not have access to that command");
				return true;
			}
			
			if (args.length != 1) {
				player.sendMessage(ChatColor.RED + "[BlockRespawn] " + ChatColor.WHITE + "Usage: /blockRespawn {setblock, reload}");
				return false;
			}
			if (args[0].equals("setblock")) {
				this.blockClickEnabled.put(player, Boolean.valueOf(true));
				player.sendMessage(ChatColor.RED + "[BlockRespawn] " + ChatColor.WHITE + "Right-click at the block you want to be respawned!");
			} else if (args[0].equals("reload")) {
				reloadConfig();
				player.sendMessage(ChatColor.RED + "[BlockRespawn] " + ChatColor.WHITE + "The config file has been reloaded!");
			} else {
				player.sendMessage(ChatColor.RED + "[BlockRespawn] commands:");
				player.sendMessage(ChatColor.WHITE + "/blockRespawn setblock " + ChatColor.GRAY + "Will set the block that will be respawned.");
				player.sendMessage(ChatColor.WHITE + "/blockRespawn reload " + ChatColor.GRAY + "Reloads the config file.");
			}
			
			return true;
		}
		
		return false;
	}

	private void startRespawnTimer() {
		int configSec = getConfig().getInt("RespawnTime.Second");
		int configMin = getConfig().getInt("RespawnTime.Minute");
		int configHour = getConfig().getInt("RespawnTime.Hour");

		if (configSec != 0) {
			longSec = configSec * 20;
		}
		
		if (configMin != 0) {
			longMin = configMin * 1200;
		}
		
		if (configHour != 0) {
			longHour = configHour * 72000;
		}
		
		long finalTick = longSec + longMin + longHour;

		respawnTimer = getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable()
		{
			public void run() {
				int blockID = BlockRespawn.this.getConfig().getInt("BlockInfo.BlockID");
				int blockType = BlockRespawn.this.getConfig().getInt("BlockInfo.Type");
				int blockX = BlockRespawn.this.getConfig().getInt("BlockInfo.X");
				int blockY = BlockRespawn.this.getConfig().getInt("BlockInfo.Y");
				int blockZ = BlockRespawn.this.getConfig().getInt("BlockInfo.Z");
				Location blockLocation = new Location(Bukkit.getWorld(BlockRespawn.this.getConfig().getString("BlockInfo.World")), blockX, blockY, blockZ);
				Block createdBlock = blockLocation.getWorld().getBlockAt(blockLocation);
				
				if (createdBlock.isEmpty()) {
					createdBlock.setTypeIdAndData(blockID, (byte)blockType, false);
				}
			}
		}
		, finalTick);
	}
}